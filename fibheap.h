#ifndef _FIBONACCI_HEAP_
#define _FIBONACCI_HEAP_

#include <stdexcept>
#include <unordered_map>
#include <iostream>

using namespace std;

template <class T>
class fibheap {
protected:
	struct Node {
		T elem;
		int deg;
		bool marked;
		Node* next;
		Node* prev;
		Node* father;
		Node* child;		//Desde child puedes acceder a otros hijos child->next
		Node(T e, Node* n, Node* p, Node* f, Node* c, int d = 0) : elem(e), deg(d), marked(false), next(n), prev(p), father(f), child(c) {};	//Constructora de nodos
	};
	int max_deg;
	Node* min;		//Puntero al minimo
	Node* ghost;	//Nodo fantasma de la lista
public:
	//Constructor
	fibheap() {
		ghost = new Node(NULL, nullptr, nullptr, nullptr, nullptr);
		ghost->next = ghost;
		ghost->prev = ghost;
		min = nullptr;
		max_deg = 0;
	}

	//Destructor
	~fibheap() {
		Node* x = ghost->next;
		if (x != ghost) {
			do {
				Node* next = x->next;
				while (x->child != nullptr) {
					Node* child = x->child;
					x->child = child->next;
					delete child;
				}
				delete x;
				x = next;

			} while (x != ghost);
		}
		delete ghost;
	}

	//Constructor por copia

	//Operaciones obligatorias
	T getMin() const {	//Devuelve el minimo en O(1)
		if (min == nullptr)
			throw domain_error("Monticulo vacio");
		return min->elem;
	}

	Node* insercion(const T e) {	//Inserta un nuevo nodo en O(1)
		Node* i = new Node(e, ghost->next, ghost, nullptr, nullptr);
		if (empty())
			min = i;
		else if (e < min->elem)
			min = i;
		ghost->next->prev = i;
		ghost->next = i;
		return i;
	}

	Node* union_f(Node* l, Node* r) {		//Unifica dos montículos en O(1)
		if (l == nullptr)
			return r;
		if (r == nullptr)
			return l;
		Node* m = l;
		Node* M = r;
		if (l->elem > r->elem) {	//M mayor, m menor comparando la raiz del arbol binomial
			M = l;
			m = r;
		}
		//Desmarcamos el mayor
		M->marked = false;
		//Sacamos M de la lista
		M->prev->next = M->next;
		M->next->prev = M->prev;
		M->prev = nullptr;
		M->next = nullptr;
		//Lo introducimos entre los hijos de m
		M->father = m;
		if (m->child != nullptr) {
			m->child->prev = M;
			M->next = m->child;
		}
		m->child = M;
		return m;
	}

	Node* decrecer_clave(Node* n, const int dec) {		//Reduce el valor del elemento de un nodo con coste amortizado O(1)
		if (dec > n->elem)
			throw domain_error("El nuevo elemento no puede ser mayor que el actual");
		n->elem -= dec;
		Node* f = n->father;
		if (f != nullptr && n->elem < f->elem) {
			cut(f, n);
			cascading_cut(f);
		}
		if (n->elem < min->elem)
			min = n;
		return n;	//Devuelve puntero
	}

	Node* delete_min() {		//Borra el minimo con coste amortizado O(log(n))
		if (empty())
			return ghost;
		Node* x = min->child;
		min->child = nullptr;
		while (x != nullptr) {	//Añade todos los hijos del arbol minimo a la lista
			Node* y = x;
			x = x->next;
			y->father = nullptr;
			y->next = ghost->next;
			y->prev = ghost;
			ghost->next->prev = y;
			ghost->next = y;
			y->marked = false;
		}
		min->next->prev = min->prev;
		min->prev->next = min->next;
		delete(min);
		min = consolidate();
		return min;	//Devuelve un puntero al nuevo minimo
	}

	//Mostrar la lista principal del montículo
	void print() const {
		Node* i = ghost->next;
		while (i != ghost) {
			cout << i->elem << " de grado " << i->deg << '\n';
			i = i->next;
		}
		cout << '\n';
	}

private:
	//Operaciones auxiliares
	Node* consolidate() {
		unordered_map<int, Node*> binh;
		Node* i = ghost->next;
		while (i != ghost) {	//Para todos los nodos de la lista principal
			auto it = binh.find(i->deg);
			while (it != binh.end()) {
				Node* y = it->second;
				i = union_f(i, y);		//Unifica los dos árboles del mismo grado 
				binh.erase(i->deg);	//No hay más árboles de grado d
				if (++(i->deg) > max_deg)	//Incrementa el grado de x y comprueba si es el nuevo máximo
					max_deg = i->deg;
				it = binh.find(i->deg);
			}
			binh[i->deg] = i;
			i = i->next;
		}
		min = ghost->next;
		i = min->next;
		while (i != ghost) {	//Nuevo minimo recorriendo la lista log(n) amortizado
			if (i->elem < min->elem)
				min = i;
			i = i->next;
		}
		return min;		//Devuelve un puntero al nuevo minimo
	}

	void cut(Node* f, Node* c) {
		//Sacamos a c de los hijos de f
		if (f->child = c)
			f->child = c->next;
		if (c->prev != nullptr)
			c->prev->next = c->next;
		if (c->next != nullptr)
			c->next->prev = c->prev;
		//c no tiene padre
		c->father = nullptr;
		//Añadimos c a la lista del monticulo
		c->next = ghost->next;
		ghost->next->prev = c;
		ghost->next = c;
		c->prev = ghost;
		//Desmarcamos c
		c->marked = false;
		//TODO quiza no debe devolver nada
	}

	void cascading_cut(Node* l) {
		Node* f = l->father;
		if (f != nullptr) {
			l->marked = false;
			f->marked = true;
		}
		else {
			cut(f, l);
			cascading_cut(f);
		}
	}

	bool empty() const {
		return ghost->next == ghost;
	}
};

#endif